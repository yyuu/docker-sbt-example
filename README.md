# docker-sbt-example

A sample project to demonstrate deploying sbt based projects to Docker environment.

## Overview

This project consists from following applications.

 * dockersbt-core - a common library for web and worker
 * dockersbt-web - a web application
 * dockersbt-worker - a batch application

This sample project will create a single Docker image to run both web and worker.

## Setup

You will need to set some of environment variables to build/run application.

```
$ cp dotenv .env
$ vi .env
```

## Build

To build base image. This will generate a Docker image tagged as `yyuu/docker-sbt`.

```
$ rake setup
```

To build application. This will generate a Docker image tagged as `yyuu/app`.

```
$ rake build
```

This project is using [heroku-buildpack-scala](https://github.com/heroku/heroku-buildpack-scala) to build sbt projects.

## Run

Run web from the built Docker image.

```
$ docker run yyuu/app
```

Run worker from the built Docker image.

```
$ docker run yyuu/app worker
```

### License

(The MIT license)

* Copyright (c) 2014 Yamashita, Yuu <<peek824545201@gmail.com>>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
