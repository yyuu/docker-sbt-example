package controllers

import org.bitbucket.yyuu.dockersbt.core.DockerSbtCore
import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    Logger.info("index")
    Ok(views.html.index(s"The ultimate question of Life, the Universe, and Everything is ${DockerSbtCore.lifeUniverseEverything}."))
  }

}
