package org.bitbucket.yyuu.dockersbt.core

object DockerSbtCore {
  val ANSWER = 42

  def lifeUniverseEverything() = ANSWER
}
