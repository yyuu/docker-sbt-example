
require "shellwords"
require "tempfile"

namespace :docker do
  task :bootstrap do
    sh "docker build -t yyuu/sbt-base bootstrap"
  end

  task :build do
    cidfile = Tempfile.new("docker-cid").path

    sh "docker build -t yyuu/sbt-app ."

    ## Retrieve ${CACHE_DIR} from built image
    sh "rm -f #{cidfile.shellescape}"
    sh "docker run --cidfile=#{cidfile.shellescape} --entrypoint=true yyuu/sbt-app"
    container_id = File.read(cidfile).strip
    sh "docker cp #{container_id.shellescape}:/cache/app .cache"
    sh "docker stop #{container_id.shellescape} 1>/dev/null"
    sh "docker rm #{container_id.shellescape} 1>/dev/null"
  end

  task :run do
    process_type = ENV.fetch("PROCESS_TYPE", "web")
    hostPorts = ENV.fetch("PORT", "5000").split.map { |port| port.to_i }
    envfile = Tempfile.new("docker-env").path

    containerPort = 5000 # TODO: shoud be a variable
    hostPorts.each_with_index do |hostPort, i|
      name = "#{process_type}#{i+1}"
      sh "docker run --detach --env=PORT=#{containerPort} --env-file=#{envfile.shellescape} --name=#{name.shellescape} --publish=#{hostPort}:#{containerPort} yyuu/sbt-app #{process_type.shellescape}"
    end

    sh "rm -f #{envfile.shellescape}"
  end

  task :default => [ "build" ]
end
