package org.bitbucket.yyuu.dockersbt.worker

import org.bitbucket.yyuu.dockersbt.core.DockerSbtCore

object DockerSbtWorker {
  def main(args: Array[String]) {
    println(s"The ultimate question of Life, the Universe, and Everything is ${DockerSbtCore.lifeUniverseEverything}.")
  }
}
