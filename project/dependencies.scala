import sbt._
import Keys._

object Dependencies {
  val rootDependencies = Seq()

  val coreDependencies = rootDependencies ++ Seq(
    "org.json4s" %% "json4s-native" % "3.2.9"
  )

  val webDependencies = rootDependencies ++ Seq(
    "com.typesafe.play" %% "play-jdbc" % play.core.PlayVersion.current,
    "com.typesafe.play" %% "anorm" % play.core.PlayVersion.current,
    "com.typesafe.play" %% "play-cache" % play.core.PlayVersion.current,
    "com.typesafe.play" %% "play-ws" % play.core.PlayVersion.current
  )

  val workerDependencies = rootDependencies
}
