import sbt._
import Keys._

object DockerSbtBuild extends Build {
  lazy val nexusSettings = Seq(
//  credentials += Credentials(
//    "Sonatype Nexus Repository Manager",
//    "nexus.example.com",
//    Option(System.getenv("NEXUS_USERNAME")).getOrElse(""),
//    Option(System.getenv("NEXUS_PASSWORD")).getOrElse("")
//  ),
//  publishTo <<= version { (v: String) =>
//    if (v.trim.endsWith("SNAPSHOT")) {
//      Some("Snapshots" at "http://nexus.example.com/nexus/content/repositories/snapshots")
//    } else {
//      Some("Releases" at  "http://nexus.example.com/nexus/content/repositories/releases")
//    }
//  },
//  resolvers += "Sonatype Nexus" at "http://nexus.example.com/nexus/content/groups/public/"
  )

  lazy val rootSettings = net.virtualvoid.sbt.graph.Plugin.graphSettings ++ nexusSettings ++ Seq(
    organization := "org.bitbucket.yyuu",
    scalaVersion := "2.11.2"
  )

  lazy val coreSettings = rootSettings ++ Seq(
    libraryDependencies ++= Dependencies.coreDependencies
  )

  lazy val webSettings = rootSettings ++ Seq(
    libraryDependencies ++= Dependencies.webDependencies
  )

  lazy val workerSettings = rootSettings ++ Seq(
    libraryDependencies ++= Dependencies.workerDependencies
  )

  lazy val root = (project in file("."))
                  .aggregate(core, web, worker)
                  .settings(rootSettings: _*)

  lazy val core = (project in file("dockersbt-core"))
                  .settings(coreSettings: _*)

  lazy val web = (project in file("dockersbt-web"))
                  .dependsOn(core)
                  .enablePlugins(play.PlayScala)
                  .settings(webSettings: _*)

  lazy val worker = (project in file("dockersbt-worker"))
                  .dependsOn(core)
                  .settings(com.typesafe.sbt.SbtNativePackager.packageArchetype.java_server: _*)
                  .settings(workerSettings: _*)
}
